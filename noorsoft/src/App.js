import React, { Component } from "react";
import "./App.css";
import Table from './Component/Table';

class App extends React.Component {
   constructor(props) {
      super(props);
      this.state = {checked: true};
      this.handleChange = this.handleChange.bind(this);
   }

   handleChange () {
      this.setState({
          checked: !this.state.checked
      });
   };

   addAlert = () => {
      alert('Nice work!')
   }

   render() {

         let message;
         if(this.state.checked) {
             message = 'Выбран';
         } else {
             message = 'Не выбран';
         }

   return(
       <div>
          <div>
              <Table name ="Jendy" age="22"/>
              <Table name ="Nasti" age="23"/>
              <button onClick={this.addAlert}>Send</button>
          </div>

          <div>
              <input type="checkbox" onChange={this.handleChange} defaultChecked={this.state.checked}/>
             <p>Chekbox {message}</p>
          </div>

          <div>
          </div>
       </div>
   );}
}

export default App;
